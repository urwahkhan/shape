/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
 class Circle extends Shape {

public Circle(double radius){
    this.radius=radius;
    
}

    double getArea() {
       area = Math.PI*radius*radius;
        return area;
    }

    double getPerimeter() {
      perimeter= 2*Math.PI*radius;
      return perimeter;
    }
    
}
