/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class ShapesSimulation {
    public static void main (String[]args){
        Circle c1=new Circle(8);
        System.out.println("Circle:"+" Radius:"+c1.radius+" Area:"+c1.getArea()+" Perimeter:"+c1.getPerimeter());
        
        Square s1=new Square(6);
        System.out.println("Square:"+" Value:"+s1.value+" Area:"+s1.getArea()+" Perimeter:"+s1.getPerimeter());
        
    }
}
