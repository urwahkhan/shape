/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class Square extends Shape {
    public Square(double value){
this.value=value;
    }

    @Override
    double getArea() {
        area= value*value;
        return area;
    }

    @Override
    double getPerimeter() {
        perimeter= (2*value)+(value*2);
        return perimeter;
    }
    
}
