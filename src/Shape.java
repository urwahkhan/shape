/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
abstract class Shape {
   double area;
   double perimeter;
   double radius;
   double value;
    abstract double getArea();
    abstract double getPerimeter();
}
